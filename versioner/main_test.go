package main

import (
	"fmt"
	"os"
	"strings"
)

func flags(bno, pkg, pfx, tim string, noSemV bool) {
	flagBno = bno
	flagPkg = pkg
	flagPfx = pfx
	flagTim = tim
	flagNoSemVer = noSemV
}

func ExampleSemVer() {
	flags("build_no", "test", "tst", "", false)
	atts := readInput(strings.NewReader(`major=1
minor=2
patch=3`))
	fmt.Println("### version.go:")
	chg := writeOutput(os.Stdout, atts)
	fmt.Println("### changed atts:")
	writeInput(os.Stdout, chg)
	// Output:
	// ### version.go:
	// package test
	//
	// const (
	// 	tstMajor   = 1
	// 	tstMinor   = 2
	// 	tstPatch   = 3
	// 	tstBuildNo = 1
	// )
	// ### changed atts:
	// major=1
	// minor=2
	// patch=3
	// build_no=1
}

// func TestSemVerFails(t *testing.T) {
// 	defer func() {
// 		if p := recover(); p != nil {
// 			t.Fatal("violation of SemVer not fatal")
// 		} else {
// 			t.Fatal("violation of SemVer not detected")
// 		}
// 	}()
// 	readInput(strings.NewReader(`Major=0
// minor=1`))
// }
