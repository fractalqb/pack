package main

import (
	"bufio"
	"flag"
	"fmt"
	"go/ast"
	"go/format"
	"go/token"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"git.fractalqb.de/fractalqb/nmconv"
	"git.fractalqb.de/fractalqb/qblog"
)

//go:generate versioner -bno build_no VERSION version.go

var log = qblog.New(&qblog.DefaultConfig).WithGroup("versioner")

var mkName = nmconv.Conversion{
	Denorm: nmconv.Camel1Up,
	Norm:   nmconv.Unsep("_"),
}

var (
	semVerNoPattern = regexp.MustCompile(`^([0-9]|([1-9][0-9]*))$`)
	semVerIdent     = regexp.MustCompile(`^[0-9a-zA-Z-]+$`)

	flagBno      string
	flagPkg      string
	flagPfx      string
	flagTim      string
	flagNoSemVer bool
	flagTag      bool
)

func logFatal(msg string, args ...any) {
	log.Error(msg, args...)
	os.Exit(1)
}

func main() {
	flag.StringVar(&flagBno, "bno", "", "use build number attribute")
	flag.StringVar(&flagPkg, "pkg", "main", "set the package name for generated file")
	flag.StringVar(&flagPfx, "p", "", "set name prefix for version constants")
	flag.StringVar(&flagTim, "t", "", "generate timestamp attribute")
	flag.BoolVar(&flagNoSemVer, "s", false, "disable check for SemVer convetions (https://semver.org)")
	flag.BoolVar(&flagTag, "tag", false, "Output version tag from VERSION file")
	flag.Usage = usage
	flag.Parse()
	if flagTag {
		printTag()
		return
	}
	inNm := flag.Arg(0)
	if len(inNm) == 0 {
		logFatal("missing input file argument")
	}
	ouNm := flag.Arg(1)
	if len(ouNm) == 0 {
		logFatal("missing output file argument")
	}
	attls := readInFile(inNm)
	attls = writeOutFile(ouNm, attls)
	if attls != nil {
		writeInFile(inNm, attls)
	}
}

func checkSemVerNo(k, v string) {
	if !semVerNoPattern.MatchString(v) {
		logFatal("`num` is not a valid SemVer number for `part`",
			`num`, v,
			`part`, k,
		)
	}
}

func checkSemVerId(k, v string) {
	if !semVerIdent.MatchString(v) {
		logFatal("`id` is not a valid SemVer identifyer for `tag`",
			`id`, v,
			`tag`, k,
		)
	}
}

type att struct {
	key, val string
}

func readInFile(filename string) []att {
	f, err := os.Open(filename)
	if err != nil {
		logFatal(err.Error())
	}
	defer f.Close()
	return readInput(f)
}

func readInput(rd io.Reader) (res []att) {
	scn := bufio.NewScanner(rd)
	var semMajor, semMinor, semPatch bool
	for scn.Scan() {
		line := scn.Text()
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		sep := strings.IndexRune(line, '=')
		if sep > 0 {
			k, v := line[:sep], line[sep+1:]
			switch strings.ToLower(k) {
			case "major":
				semMajor = true
				if !flagNoSemVer {
					checkSemVerNo(k, v)
				}
			case "minor":
				semMinor = true
				if !flagNoSemVer {
					checkSemVerNo(k, v)
				}
			case "patch":
				semPatch = true
				if !flagNoSemVer {
					checkSemVerNo(k, v)
				}
			default:
				if !flagNoSemVer {
					checkSemVerId(k, v)
				}
			}
			a := att{key: k, val: v}
			res = append(res, a)
		} else {
			logFatal("syntax error in `line`", `line`, line)
		}
	}
	if !flagNoSemVer {
		if !semMajor {
			logFatal("missing major version, set value 'major'")
		}
		if !semMinor {
			logFatal("missing minor version, set value 'minor'")
		}
		if !semPatch {
			logFatal("missing patch version, set value 'patch'")
		}
	}
	return res
}

func writeInFile(filename string, attls []att) {
	tmpnm := filename + "~"
	f, err := os.Create(tmpnm)
	if err != nil {
		logFatal(err.Error())
	}
	defer func() {
		if f != nil {
			f.Close()
		}
	}()
	writeInput(f, attls)
	err = f.Close()
	f = nil
	if err != nil {
		logFatal(err.Error())
	}
	os.Rename(tmpnm, filename)
}

func writeInput(wr io.Writer, attls []att) {
	for _, a := range attls {
		fmt.Fprintf(wr, "%s=%s\n", a.key, a.val)
	}
}

func needQuote(s string) bool {
	if s == "true" || s == "false" {
		return false
	} else if _, err := strconv.ParseInt(s, 10, 64); err == nil {
		return false
	} else if _, err := strconv.ParseUint(s, 10, 64); err == nil {
		return false
	} else if _, err := strconv.ParseFloat(s, 64); err == nil {
		return false
	}
	return true
}

func quote(s string) string {
	if needQuote(s) {
		return "`" + s + "`"
	}
	return s
}

func mkVSpec(name, value string, kind token.Token) *ast.ValueSpec {
	res := &ast.ValueSpec{
		Names: []*ast.Ident{&ast.Ident{Name: flagPfx + name}},
		Values: []ast.Expr{
			&ast.BasicLit{
				Kind:  kind,
				Value: quote(value),
			},
		},
	}
	return res
}

func makeAst(atts []att) (*ast.File, []att) {
	var vals []ast.Spec
	hadBNo := false
	for i, a := range atts {
		atnm := mkName.Convert(a.key)
		atkind := token.STRING
		atval := a.val
		if a.key == flagBno {
			n, err := strconv.Atoi(a.val)
			if err != nil {
				logFatal("invalid build `number` in `attribute`",
					`number`, a.val,
					`attribute`, a.key,
				)
			}
			atval = strconv.Itoa(n + 1)
			atkind = token.INT
			hadBNo = true
			atts[i].val = atval
		}
		spec := mkVSpec(atnm, atval, atkind)
		vals = append(vals, spec)
	}
	if !hadBNo {
		if len(flagBno) > 0 {
			atts = append(atts, att{key: flagBno, val: "1"})
			atnm := mkName.Convert(flagBno)
			spec := mkVSpec(atnm, "1", token.INT)
			vals = append(vals, spec)
		} else {
			atts = nil
		}
	}
	if len(flagTim) > 0 {
		spec := mkVSpec(flagTim, time.Now().Format(time.RFC3339), token.STRING)
		vals = append(vals, spec)
	}
	res := &ast.File{
		Package: 1,
		Name:    &ast.Ident{Name: flagPkg},
		Decls: []ast.Decl{
			&ast.GenDecl{
				Tok:   token.CONST,
				Specs: vals,
			},
		},
	}
	return res, atts
}

func writeOutFile(filename string, atts []att) (bnoChanged []att) {
	f, err := os.Create(filename)
	if err != nil {
		logFatal(err.Error())
	}
	defer f.Close()
	return writeOutput(f, atts)
}

func writeOutput(wr io.Writer, atts []att) (bnoChanged []att) {
	vast, bnoChanged := makeAst(atts)
	fset := token.NewFileSet()
	format.Node(wr, fset, vast)
	return bnoChanged
}

func printTag() {
	inDir, err := os.Getwd()
	if err != nil {
		logFatal(err.Error())
	}
	inDir, err = filepath.Abs(inDir)
	if err != nil {
		logFatal(err.Error())
	}
	var vFile string
	for {
		vFile = filepath.Join(inDir, "VERSION")
		if _, err := os.Stat(vFile); err == nil {
			break
		}
		tmp := filepath.Dir(inDir)
		if tmp == inDir {
			tmp, _ = os.Getwd()
			logFatal("Cannot find VERSION file for `dir`", `dir`, tmp)
		}
		inDir = tmp
	}
	atts := readInFile(vFile)
	checkSemVerNo(atts[0].key, atts[0].val)
	checkSemVerNo(atts[1].key, atts[1].val)
	checkSemVerNo(atts[2].key, atts[2].val)
	fmt.Printf("v%s.%s.%s\n", atts[0].val, atts[1].val, atts[2].val)
}

func usage() {
	fmt.Fprintf(os.Stderr, "%s %d.%d.%d-%s+%d (%s)\n",
		os.Args[0], Major, Minor, Patch, Quality, BuildNo,
		runtime.Version())
	fmt.Fprintln(os.Stderr, "Usage: [flags] [input output]")
	fmt.Fprintln(os.Stderr, "Flags:")
	flag.PrintDefaults()
}
