package appdir

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

type Path string

const (
	AppRunName  = "AppRun"
	DirIconName = ".DirIcon"
	DesktopExt  = ".desktop"

	Root     Path = "."
	BinDir   Path = "usr/bin"
	LibDIr   Path = "usr/lib"
	ShareDir Path = "usr/share"
)

type D struct {
	Dir     string
	AppName string
	icon    string
}

func New(dir string, app string) (D, error) {
	if err := os.MkdirAll(dir, 0777); err != nil {
		return D{}, err
	}
	return D{Dir: dir, AppName: app}, nil
}

func (d D) In(p Path, name ...string) string {
	elem := []string{d.Dir, string(p)}
	return filepath.Join(append(elem, name...)...)
}

func (d D) MkDirs(dirs ...any) error {
	mkdir := func(dir string) error {
		dir = filepath.Join(d.Dir, dir)
		return os.MkdirAll(dir, 0777)
	}
	for _, d := range dirs {
		switch d := d.(type) {
		case Path:
			if err := mkdir(string(d)); err != nil {
				return err
			}
		case string:
			if err := mkdir(d); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unsupported path type %T", d)
		}
	}
	return nil
}

func (d D) SetAppRun(inBinDir string) error {
	exe := d.In(BinDir, inBinDir)
	if _, err := os.Stat(exe); err != nil {
		return err
	}
	// TODO check if executable
	relPath, err := filepath.Rel(d.Dir, exe)
	if err != nil {
		return err
	}
	return os.Symlink(relPath, d.In(Root, AppRunName))
}

func (d *D) SetIcon(inShareDir string) error {
	icn := d.In(ShareDir, inShareDir)
	if _, err := os.Stat(icn); err != nil {
		return err
	}
	relPath, err := filepath.Rel(d.Dir, icn)
	if err != nil {
		return err
	}
	if ext := filepath.Ext(relPath); ext != "" {
		relPath = strings.TrimSuffix(relPath, ext)
	}
	d.icon = "/" + relPath
	return nil
}

func (d D) WriteDesktop(dsk Desktop) error {
	w, err := os.Create(d.In(Root, d.AppName+DesktopExt))
	if err != nil {
		return err
	}
	defer w.Close()
	if dsk.Name == "" {
		dsk.Name = d.AppName
	}
	if dsk.Type == "" {
		dsk.Type = DskTypeApplication
	}
	if d.icon != "" {
		dsk.icon = d.icon
	}
	return dsk.Write(w)
}
