package appdir

import (
	"fmt"
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/pack"
)

func Example() {
	os.RemoveAll("tstad")
	d := must.Ret(New("tstad", "Test"))
	must.Do(d.MkDirs(BinDir, ShareDir))
	must.Do(pack.CopyFile(d.In(BinDir, "hw"), "appdir", nil))
	must.Do(pack.CopyToDir(d.In(ShareDir), nil, "icon.png"))
	must.Do(d.SetAppRun("hw"))
	must.Do(d.SetIcon("icon.png"))
	must.Do(d.WriteDesktop(Desktop{
		Categories: []string{DskCatUtility},
	}))
	fmt.Println("done")
	// Output:
	// done
}
