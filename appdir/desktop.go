package appdir

import (
	"fmt"
	"io"
	"strings"
)

// https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html

type DesktopType string

const (
	DskTypeApplication = "Application"
	DskTypeLink        = "Link"
	DskTypeDirectory   = "Directory"
)

// https://specifications.freedesktop.org/menu-spec/latest/apa.html#main-category-registry
const (
	DskCatAudioVideo  = "AudioVideo"
	DskCatAudio       = "Audio"
	DskCatVideo       = "Video"
	DskCatDevelopment = "Development"
	DskCatEducation   = "Education"
	DskCatGame        = "Game"
	DskCatGraphics    = "Graphics"
	DskCatNetwork     = "Network"
	DskCatOffice      = "Office"
	DskCatScience     = "Science"
	DskCatSettings    = "Settings"
	DskCatSystem      = "System"
	DskCatUtility     = "Utility"
)

type Desktop struct {
	Type       DesktopType
	Name       string
	Categories []string
	icon       string
}

func (dsk *Desktop) Write(w io.Writer) error {
	fmt.Fprintln(w, "[Desktop Entry]")
	if s := strings.TrimSpace(string(dsk.Type)); s != "" {
		fmt.Fprintf(w, "Type=%s\n", s)
	}
	if s := strings.TrimSpace(dsk.Name); s != "" {
		fmt.Fprintf(w, "Name=%s\n", s)
	}
	if len(dsk.Categories) > 0 {
		io.WriteString(w, "Categories=")
		for i, cat := range dsk.Categories {
			if i > 0 {
				io.WriteString(w, ";")
			}
			io.WriteString(w, cat)
		}
		fmt.Fprintln(w)
	}
	if dsk.icon != "" {
		fmt.Fprintf(w, "Icon=%s\n", dsk.icon)
	}
	return nil
}
