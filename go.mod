module git.fractalqb.de/fractalqb/pack

require (
	git.fractalqb.de/fractalqb/nmconv v1.0.2
	git.fractalqb.de/fractalqb/qblog v0.14.3
)

require (
	git.fractalqb.de/fractalqb/eloc v0.0.0-20231227105451-985ff6d3257d
	git.fractalqb.de/fractalqb/sllm/v3 v3.0.0-beta.2 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
)

go 1.21.5
