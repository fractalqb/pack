package ospath

import (
	"os/user"
	"path/filepath"
)

func homeDir(usr string) string {
	if u, err := user.Lookup(string(usr)); err == nil {
		return u.HomeDir
	}
	return filepath.Join(`C:\Users`, usr)
}
