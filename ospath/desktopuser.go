// +build !windows

package ospath

import (
	"path/filepath"
)

// References
// https://www.freedesktop.org/software/systemd/man/file-hierarchy.html

type DesktopUserApp string

func NewDesktopUserApp(username string) DesktopUserApp {
	return DesktopUserApp(username)
}

func (u DesktopUserApp) Bin(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append(
		[]string{home, ".local", "bin"},
		app...,
	)...)
	return res
}

func (u DesktopUserApp) Lib(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append(
		[]string{home, ".local", "lib"},
		app...,
	)...)
	return res
}

func (u DesktopUserApp) Doc(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append(
		[]string{home, ".local", "doc"},
		app...,
	)...)
	return res
}

func (u DesktopUserApp) LocalData(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append(
		[]string{home, ".local", "share"},
		app...,
	)...)
	return res
}

func (u DesktopUserApp) RoamingData(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append([]string{home, ".config"}, app...)...)
	return res
}
