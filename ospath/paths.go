package ospath

import (
	"io/fs"
	"os"
	"path/filepath"
)

func KeepAbs(path string, rel func(string) string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return rel(path)
}

func ProvideDir(perm fs.FileMode, forfile string) (file string, err error) {
	dir := filepath.Dir(forfile)
	_, err = os.Stat(dir)
	if os.IsNotExist(err) {
		err = os.MkdirAll(dir, perm)
	}
	return forfile, err
}

func MustDir(perm fs.FileMode, forfile string) (file string) {
	res, err := ProvideDir(perm, forfile)
	if err != nil {
		panic(err)
	}
	return res
}

type AppPath int

func (ap AppPath) All(flags AppPath) bool {
	return (ap & flags) == flags
}

func (ap AppPath) Any(flags AppPath) bool {
	return (ap & flags) != 0
}

const (
	BinPath AppPath = (1 << iota)
	LibPath
	DocPath
	LocalDataPath
	RoamingDataPath

	appPathEnd
)

const InstallPaths = BinPath | LibPath | DocPath
const DataPaths = LocalDataPath | RoamingDataPath
const AllPaths = InstallPaths | DataPaths

type AppPaths interface {
	Bin(app ...string) string
	Lib(app ...string) string
	Doc(app ...string) string
	LocalData(app ...string) string
	RoamingData(app ...string) string
}

type App struct {
	app []string
	set AppPaths
}

func NewApp(set AppPaths, app ...string) App {
	return App{
		app: app,
		set: set,
	}
}

func (a App) CheckDirs(flags AppPath) (res AppPath, err error) {
	if flags.All(BinPath) {
		if ok, err := checkDir(a.Bin()); err != nil {
			return 0, err
		} else if ok {
			res |= BinPath
		}
	}
	if flags.All(LibPath) {
		if ok, err := checkDir(a.Lib()); err != nil {
			return 0, err
		} else if ok {
			res |= LibPath
		}
	}
	if flags.All(DocPath) {
		if ok, err := checkDir(a.Doc()); err != nil {
			return 0, err
		} else if ok {
			res |= DocPath
		}
	}
	if flags.All(LocalDataPath) {
		if ok, err := checkDir(a.LocalData()); err != nil {
			return 0, err
		} else if ok {
			res |= LocalDataPath
		}
	}
	if flags.All(RoamingDataPath) {
		if ok, err := checkDir(a.RoamingData()); err != nil {
			return 0, err
		} else if ok {
			res |= RoamingDataPath
		}
	}
	return res, nil
}

func (a App) ProvideDirs(perm fs.FileMode, flags AppPath) error {
	if flags.All(BinPath) {
		if err := os.MkdirAll(a.Bin(), perm); err != nil {
			return err
		}
	}
	if flags.All(LibPath) {
		if err := os.MkdirAll(a.Lib(), perm); err != nil {
			return err
		}
	}
	if flags.All(DocPath) {
		if err := os.MkdirAll(a.Doc(), perm); err != nil {
			return err
		}
	}
	if flags.All(LocalDataPath) {
		if err := os.MkdirAll(a.LocalData(), perm); err != nil {
			return err
		}
	}
	if flags.All(RoamingDataPath) {
		if err := os.MkdirAll(a.RoamingData(), perm); err != nil {
			return err
		}
	}
	return nil
}

func checkDir(dir string) (bool, error) {
	s, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return s.IsDir(), nil
}

func (a App) Bin(rel ...string) string {
	d := a.set.Bin(a.app...)
	return filepath.Join(append([]string{d}, rel...)...)
}

func (a App) BinPath(rel string) string {
	d := a.set.Bin(a.app...)
	return filepath.Join(d, rel)
}

func (a App) Lib(rel ...string) string {
	d := a.set.Lib(a.app...)
	return filepath.Join(append([]string{d}, rel...)...)
}

func (a App) LibPath(rel string) string {
	d := a.set.Lib(a.app...)
	return filepath.Join(d, rel)
}

func (a App) Doc(rel ...string) string {
	d := a.set.Doc(a.app...)
	return filepath.Join(append([]string{d}, rel...)...)
}

func (a App) DocPath(rel string) string {
	d := a.set.Doc(a.app...)
	return filepath.Join(d, rel)
}

func (a App) LocalData(rel ...string) string {
	d := a.set.LocalData(a.app...)
	return filepath.Join(append([]string{d}, rel...)...)
}

func (a App) LocalDataPath(rel string) string {
	d := a.set.LocalData(a.app...)
	return filepath.Join(d, rel)
}

func (a App) RoamingData(rel ...string) string {
	d := a.set.RoamingData(a.app...)
	return filepath.Join(append([]string{d}, rel...)...)
}

func (a App) RoamingDataPath(rel string) string {
	d := a.set.RoamingData(a.app...)
	return filepath.Join(d, rel)
}
