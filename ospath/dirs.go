package ospath

import (
	"os"
	"path/filepath"
)

type SingleDir string

func ExeDir() SingleDir {
	d := os.Args[0]
	d = filepath.Dir(d)
	return SingleDir(d)
}

func (d SingleDir) Bin(_ ...string) string { return string(d) }

func (d SingleDir) Lib(_ ...string) string { return string(d) }

func (d SingleDir) Doc(_ ...string) string { return string(d) }

func (d SingleDir) LocalData(_ ...string) string { return string(d) }

func (d SingleDir) RoamingData(_ ...string) string { return string(d) }

type DirTree struct {
	base           string
	RelBin         string
	RelLib         string
	RelDoc         string
	RelLocalData   string
	RelRoamingData string
}

func NewDirTree(basedir string, init *DirTree) *DirTree {
	res := new(DirTree)
	if init != nil {
		*res = *init
	}
	res.base = basedir
	return res
}

func ExeDirTree(init *DirTree) *DirTree {
	d := os.Args[0]
	d = filepath.Dir(d)
	return NewDirTree(d, init)
}

func (d *DirTree) Bin(_ ...string) string {
	p := d.RelBin
	return filepath.Join(string(d.base), p)
}

func (d *DirTree) Lib(_ ...string) string {
	p := d.RelLib
	if p == "" {
		p = "lib"
	}
	return filepath.Join(string(d.base), p)
}

func (d *DirTree) Doc(_ ...string) string {
	p := d.RelDoc
	if p == "" {
		p = "doc"
	}
	return filepath.Join(string(d.base), p)
}

func (d *DirTree) LocalData(_ ...string) string {
	p := d.RelLocalData
	if p == "" {
		p = "data"
	}
	return filepath.Join(string(d.base), p)
}

func (d *DirTree) RoamingData(_ ...string) string {
	p := d.RelRoamingData
	if p == "" {
		p = "roaming"
	}
	return filepath.Join(string(d.base), p)
}
