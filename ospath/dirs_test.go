// +build !windows

package ospath

import (
	"fmt"
)

func ExampleSingleDir() {
	d := SingleDir("basedir")
	app := NewApp(d, "example-app")
	fmt.Println(app.Bin("executable"))
	fmt.Println(app.Lib("sample.so"))
	fmt.Println(app.Doc("README.md"))
	fmt.Println(app.LocalData("local.data"))
	fmt.Println(app.RoamingData("roaming.data"))
	// Output:
	// basedir/executable
	// basedir/sample.so
	// basedir/README.md
	// basedir/local.data
	// basedir/roaming.data
}

func ExampleDirTree() {
	d := NewDirTree("basedir", nil)
	app := NewApp(d, "example-app")
	fmt.Println(app.Bin("executable"))
	fmt.Println(app.Lib("sample.so"))
	fmt.Println(app.Doc("README.md"))
	fmt.Println(app.LocalData("local.data"))
	fmt.Println(app.RoamingData("roaming.data"))
	// Output:
	// basedir/executable
	// basedir/lib/sample.so
	// basedir/doc/README.md
	// basedir/data/local.data
	// basedir/roaming/roaming.data
}
