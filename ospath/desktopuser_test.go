// +build !windows

package ospath

import (
	"os"
	"os/user"
	"testing"
)

var _ AppPaths = DesktopUserApp("")

func TestUserSet_AppConfigDir(t *testing.T) {
	expect := os.Getenv("HOME") + "/.config/pack/" + t.Name()
	usr, err := user.Current()
	if err != nil {
		t.Fatal(err)
	}
	got := NewDesktopUserApp(usr.Username).RoamingData("pack", t.Name())
	if got != expect {
		t.Errorf("expect '%s', got '%s'", expect, got)
	}
}

func TestUserSet_AppDataDir(t *testing.T) {
	expect := os.Getenv("HOME") + "/.local/share/pack/" + t.Name()
	usr, err := user.Current()
	if err != nil {
		t.Fatal(err)
	}
	got := NewDesktopUserApp(usr.Username).LocalData("pack", t.Name())
	if got != expect {
		t.Errorf("expect '%s', got '%s'", expect, got)
	}
}
