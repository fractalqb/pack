package ospath

import (
	"path/filepath"
)

type DesktopUserApp string

func NewDesktopUserApp(username string) DesktopUserApp {
	return DesktopUserApp(username)
}

func (u DesktopUserApp) Bin(app ...string) string {
	// home := homeDir(string(u))
	panic("NYI!")
}

func (u DesktopUserApp) Lib(app ...string) string {
	// home := homeDir(string(u))
	panic("NYI!")
}

func (u DesktopUserApp) Doc(app ...string) string {
	// home := homeDir(string(u))
	panic("NYI!")
}

func (u DesktopUserApp) LocalData(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append(
		[]string{home, "AppData", "Local"},
		app...,
	)...)
	return res
}

func (u DesktopUserApp) RoamingData(app ...string) string {
	home := homeDir(string(u))
	res := filepath.Join(append(
		[]string{home, "AppData", "Roaming"},
		app...,
	)...)
	return res
}
