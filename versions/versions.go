package versions

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
)

type SemVerPart int

const (
	SemVerMajor SemVerPart = iota
	SemVerMinor
	SemVerPatch
	SemVerPreRelease
	SemVerMeta
)

var semVerParts = []string{"major", "minor", "patch", "prerelease", "meta"}

func (svp SemVerPart) String() string {
	if svp < 0 || svp > SemVerMeta {
		return fmt.Sprintf("SemVerPart:%d", svp)
	}
	return semVerParts[svp]
}

type NumConstraint interface {
	Check(n int) error
}

type NumExact struct {
	Name string
	V    int
}

func (vnc NumExact) Check(v int) error {
	if vnc.V == v {
		return nil
	}
	return fmt.Errorf("%s number %d does not match %d", vnc.Name, v, vnc.V)
}

type NumAtLeast struct {
	Name string
	V    int
}

func (vnc NumAtLeast) Check(v int) error {
	if vnc.V <= v {
		return nil
	}
	return fmt.Errorf("%s number %d is lower than %d", vnc.Name, v, vnc.V)
}

type Constraint []NumConstraint

func (vc Constraint) Check(v ...int) error {
	vStr := func() string {
		var sb strings.Builder
		for i, n := range v {
			if i > 0 {
				sb.WriteByte('.')
			}
			fmt.Fprintf(&sb, "%d", n)
		}
		return sb.String()
	}
	if len(vc) > len(v) {
		return fmt.Errorf("version %s too unspecific, want %d parts", vStr(), len(vc))
	}
	for i, vnc := range vc {
		if err := vnc.Check(v[i]); err != nil {
			return fmt.Errorf("version %s: %s", vStr(), err)
		}
	}
	return nil
}

func SemVerConstraint(major, minor, patch int) Constraint {
	return Constraint{
		NumExact{SemVerMajor.String(), major},
		NumAtLeast{SemVerMinor.String(), minor},
		NumAtLeast{SemVerPatch.String(), patch},
	}
}

func ReadFile(name string, mandatory ...string) (map[string]string, error) {
	rd, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer rd.Close()
	res := make(map[string]string)
	scn := bufio.NewScanner(rd)
	lino := 0
	for scn.Scan() {
		line := scn.Bytes()
		lino++
		sep := bytes.IndexByte(line, '=')
		switch {
		case sep < 0:
			return res, fmt.Errorf("no '=' in line %d", lino)
		case sep < 1:
			return res, fmt.Errorf("empty name before '#' in line %d", lino)
		}
		k := string(line[:sep])
		v := string(line[sep+1:])
		res[k] = v
	}
	var miss []string
	for _, nm := range mandatory {
		if _, ok := res[nm]; !ok {
			miss = append(miss, nm)
		}
	}
	if len(miss) > 0 {
		return res, fmt.Errorf("missing: %s", strings.Join(miss, " "))
	}
	return res, nil
}
