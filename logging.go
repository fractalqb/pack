package pack

import (
	"errors"
	"fmt"
	"log/slog"

	"git.fractalqb.de/fractalqb/qblog"
)

var log = qblog.New(&qblog.DefaultConfig)

func SetLog(l *slog.Logger) { log = &qblog.Logger{Logger: l} }

func Task(name string, do func() error) (err error) {
	log.Info("begin `task`", `task`, name)
	defer func() {
		if x := recover(); x != nil {
			switch e := x.(type) {
			case error:
				err = e
			case string:
				err = errors.New(e)
			default:
				err = fmt.Errorf("error: %v", x)
			}
		}
		if err != nil {
			log.Error("`task` failed with `error`", `task`, name, `error`, err)
		} else {
			log.Info("done with `task`", `task`, name)
		}
	}()
	err = do()
	return err
}
