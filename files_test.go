package pack

import (
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"
	// "github.com/tdewolff/minify/v2"
)

func must(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err)
	}
}

func sameStrs(
	t *testing.T,
	must bool,
	act interface{},
	toStr func(interface{}) string,
	exp ...string,
) {
	actVal := reflect.ValueOf(act)
	if actVal.Len() != len(exp) {
		if must {
			t.Fatalf("different length: %d / %d %s",
				len(exp),
				actVal.Len(),
				exp)
		} else {
			t.Errorf("different length: %d / %d %s",
				len(exp),
				actVal.Len(),
				exp)
		}
		return
	}
	var acts []string
	for i := range exp {
		acts = append(acts, toStr(actVal.Index(i).Interface()))
	}
	sameStrings(t, must, exp, acts)
}

func sameStrings(t *testing.T, must bool, exp, act []string) {
	if len(exp) != len(act) {
		if must {
			t.Fatalf("NE: %v / %v", exp, act)
		} else {
			t.Errorf("NE: %v / %v", exp, act)
		}
	}
	sort.Strings(exp)
	sort.Strings(act)
	for i := range exp {
		if exp[i] != act[i] {
			if must {
				t.Fatalf("#%d NE: '%s' / '%s'", i, exp[i], act[i])
			} else {
				t.Errorf("#%d NE: '%s' / '%s'", i, exp[i], act[i])
			}
		}
	}
}

const (
	TEST_DIR = "./test"
	TEST_SRC = TEST_DIR + "/src"
)

func prepareTemp(t *testing.T) (dir string) {
	var err error
	dir, err = os.MkdirTemp(TEST_DIR, t.Name())
	if err != nil {
		t.Fatal(err)
	}
	return dir
}

func skipBak(dir string, file os.FileInfo) bool {
	return !strings.HasSuffix(file.Name(), "~")
}

func TestCollectToDir(t *testing.T) {
	temp := prepareTemp(t)
	defer os.RemoveAll(temp)
	must(t, CollectToDir(temp, TEST_SRC, nil, nil))
	ls, err := os.ReadDir(temp)
	must(t, err)
	sameStrs(t, true, ls,
		func(f interface{}) string { return f.(os.DirEntry).Name() },
		"file1.1", "file1.2", //"file1.3~",
		"file3.1", "file3.2", "file3.3")
}

func TestCollectToDir_noBak(t *testing.T) {
	temp := prepareTemp(t)
	defer os.RemoveAll(temp)
	must(t, CollectToDir(temp, TEST_SRC, skipBak, nil))
	ls, err := os.ReadDir(temp)
	must(t, err)
	sameStrs(t, true, ls,
		func(f interface{}) string { return f.(os.DirEntry).Name() },
		"file1.1", "file1.2",
		"file3.1", "file3.2", "file3.3")
}

func TestCollectToDir_skipDir(t *testing.T) {
	temp := prepareTemp(t)
	defer os.RemoveAll(temp)
	must(t, CollectToDir(temp, TEST_SRC,
		func(dir string, file os.FileInfo) bool { return file.Name() != "dir1" },
		nil))
	ls, err := os.ReadDir(temp)
	must(t, err)
	sameStrs(t, true, ls,
		func(f interface{}) string { return f.(os.DirEntry).Name() },
		"file3.1", "file3.2", "file3.3")
}

func TestCopyRecursive(t *testing.T) {
	temp := prepareTemp(t)
	defer os.RemoveAll(temp)
	must(t, CopyRecursive(temp, TEST_SRC, nil, nil))
	var ls []string
	must(t, filepath.Walk(temp, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}
		if !info.IsDir() {
			path, err := filepath.Rel(temp, path)
			if err != nil {
				t.Fatal(err)
			}
			ls = append(ls, path)
		}
		return nil
	}))
	sameStrs(t, true, ls,
		func(f interface{}) string { return f.(string) },
		"dir1/file1.1",
		"dir1/file1.2",
		//"dir1/file1.3~",
		"dir3/file3.1",
		"dir3/file3.2",
		"dir3/file3.3",
	)
}

func TestCopyRecursive_noBak(t *testing.T) {
	temp := prepareTemp(t)
	defer os.RemoveAll(temp)
	must(t, CopyRecursive(temp, TEST_SRC, skipBak, nil))
	var ls []string
	must(t, filepath.Walk(temp, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}
		if !info.IsDir() {
			path, err := filepath.Rel(temp, path)
			if err != nil {
				t.Fatal(err)
			}
			ls = append(ls, path)
		}
		return nil
	}))
	sameStrs(t, true, ls,
		func(f interface{}) string { return f.(string) },
		"dir1/file1.1",
		"dir1/file1.2",
		"dir3/file3.1",
		"dir3/file3.2",
		"dir3/file3.3",
	)
}

func TestCopyRecursive_skipDir(t *testing.T) {
	temp := prepareTemp(t)
	defer os.RemoveAll(temp)
	must(t, CopyRecursive(temp, TEST_SRC,
		func(dir string, file os.FileInfo) bool { return file.Name() != "dir1" },
		nil))
	var ls []string
	must(t, filepath.Walk(temp, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}
		if !info.IsDir() {
			path, err := filepath.Rel(temp, path)
			if err != nil {
				t.Fatal(err)
			}
			ls = append(ls, path)
		}
		return nil
	}))
	sameStrs(t, true, ls,
		func(f interface{}) string { return f.(string) },
		"dir3/file3.1",
		"dir3/file3.2",
		"dir3/file3.3",
	)
}

// func ExampleCopyNMinify() {
// 	m := minify.New()
// }
