package pack

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
)

type OsDepNames = map[string]string

var OsDepExe = OsDepNames{
	"windows": "%s.exe",
}

// CopyFile copies file with path src to file with path dst. It also tarnsfers
// the file mode from the src file to the dst file.
func CopyFile(dst, src string, osdn OsDepNames) error {
	if osdn != nil {
		if pat, ok := osdn[runtime.GOOS]; ok {
			src = fmt.Sprintf(pat, src)
			dst = fmt.Sprintf(pat, dst)
		}
	}
	df, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer df.Close()
	sf, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sf.Close()
	log.Info("copy `src` → `dst`", `src`, src, `dst`, dst)
	_, err = io.Copy(df, sf)
	if err != nil {
		return err
	}
	stat, err := os.Stat(src)
	if err != nil {
		return err
	}
	err = os.Chmod(dst, stat.Mode())
	return err
}

// CopyToDir copies a list of files to a single destination directory using
// CopyFile on each source file.
func CopyToDir(dst string, osdn OsDepNames, files ...string) error {
	for _, f := range files {
		b := filepath.Base(f)
		dst := filepath.Join(dst, b)
		err := CopyFile(dst, f, osdn)
		if err != nil {
			return err
		}
	}
	return nil
}

func walk(root string, do func(dir string, info os.FileInfo) (bool, error)) error {
	if stat, err := os.Stat(root); err != nil {
		return err
	} else if enter, err := do(filepath.Dir(root), stat); err != nil {
		return err
	} else if enter {
		log.Info("walk into `dir`", `dir`, root)
		rddir, err := os.Open(root)
		if err != nil {
			return err
		}
		defer rddir.Close()
		for infos, err := rddir.Readdir(1); err != io.EOF; infos, err = rddir.Readdir(1) {
			if err != nil {
				return err
			}
			info := infos[0]
			if info.IsDir() {
				err = walk(filepath.Join(root, info.Name()), do)
				if err != nil {
					return err
				}
			} else {
				done, err := do(root, info)
				if err != nil {
					return err
				}
				if done {
					log.Info("done with `dir` after `file`",
						`dir`, root,
						`file`, info.Name(),
					)
					return nil
				}
			}
		}
		log.Info("walk out of `dir`", `dir`, root)
	} else {
		log.Info("walk around `dir`", `dir`, root)
	}
	return nil
}

// Search file tree root for files that match filter and copy them to the single
// destination directory dst. If filter is nil all files are collected.
func CollectToDir(
	dst, root string,
	filter func(dir string, file os.FileInfo) bool,
	osdn OsDepNames,
) error {
	return walk(root, func(dir string, info os.FileInfo) (bool, error) {
		pass := true
		if filter != nil {
			pass = filter(dir, info)
		}
		switch {
		case info.IsDir():
			return pass, nil
		case pass:
			err := CopyToDir(dst, osdn, filepath.Join(dir, info.Name()))
			return false, err
		default:
			return false, nil
		}
	})
}

// CopyRecursive copies the content of the src directory into the dst directory.
// If filter is not nil only those files or subdirectories are copied for which
// the filter returns true.
func CopyRecursive(
	dst, src string,
	filter func(dir string, info os.FileInfo) bool,
	osdn OsDepNames,
) error {
	dst = filepath.Clean(dst)
	src = filepath.Clean(src)
	return walk(src, func(dir string, info os.FileInfo) (bool, error) {
		pass := true
		if filter != nil {
			pass = filter(dir, info)
		}
		switch {
		case info.IsDir():
			return pass, nil
		case pass:
			rel, err := filepath.Rel(src, dir)
			if err != nil {
				return false, err
			}
			toDir := filepath.Join(dst, rel)
			err = os.MkdirAll(toDir, 0777)
			if err != nil {
				return false, err
			}
			err = CopyToDir(toDir, osdn, filepath.Join(dir, info.Name()))
			return false, err
		default:
			return false, nil
		}
	})
}

// CopyTree copies the directory tree src into the dst directory, i.e. on succes
// the directory dst will contain one, perhaps new, directory src.
func CopyTree(
	dst, src string,
	filter func(dir string, info os.FileInfo) bool,
	osdn OsDepNames,
) error {
	dst = filepath.Join(dst, filepath.Base(src))
	err := os.Mkdir(dst, 0777)
	if err != nil {
		return err
	}
	err = CopyRecursive(dst, src, filter, osdn)
	return err
}
